<?php

/**
 * @file
 * A webform component to capture all incoming URLs from the file chooser UI.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_cloudupload() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'required' => 0,
    'extra' => array(
      'filtering' => array(
        'types' => array('txt pdf docx doc rtf'),
        'addextensions' => '',
        'size' => '400 KB',
      ),
      'scheme' => 'private',
    ),
    'title_display' => 0,
    'attributes' => array(),
    'private' => FALSE,
    'analysis' => FALSE,
  );
}

/**
 * Implementation of _webform_theme_component().
 */
function _webform_theme_cloudupload() {
  return array(
    'webform_cloud_upload_file' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_cloudupload($component) {
  $form = array();
  $form['value'] = array(
    '#type' => 'textarea',
    '#title' => t('Default value'),
    '#default_value' => $component['value'],
    '#description' => t('The default value of the field.'),
    '#cols' => 60,
    '#rows' => 5,
    '#weight' => 0,
  );

  $form['extra']['size'] = array(
    '#type' => 'textfield',
    '#title' => t('Max upload size'),
    '#default_value' => $component['extra']['filtering']['size'],
    '#description' => t('Enter the max file size a user may upload such as 2 MB or 800 KB. Your server has a max upload size of @size.', array('@size' => format_size(file_upload_max_size()))),
    '#size' => 10,
    '#parents' => array('extra', 'filtering', 'size'),
    '#weight' => 1,
  );

  $form['extra']['extensions'] = array(
    '#parents' => array('extra', 'filtering'),
    '#theme_wrappers' => array('form_element'),
    '#default_value' => $component['extra']['filtering']['types'],
    '#title' => t('Comma seperated allowed file extensions'),
    '#parents' => array('extra', 'filtering', 'types'),
    '#type' => 'textfield',
    '#weight' => 2,
  );

  $scheme_options = array();
  foreach (file_get_stream_wrappers(STREAM_WRAPPERS_WRITE_VISIBLE) as $scheme => $stream_wrapper) {
    $scheme_options[$scheme] = $stream_wrapper['name'];
  }

  $form['extra']['scheme'] = array(
    '#type' => 'radios',
    '#title' => t('Upload destination'),
    '#options' => $scheme_options,
    '#default_value' => $component['extra']['scheme'],
    '#description' => t('Private file storage has significantly more overhead than public files, but restricts file access to users who can view submissions.'),
    '#weight' => 4,
    '#access' => count($scheme_options) > 1,
  );

  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_cloudupload($component, $value = NULL, $filter = TRUE) {
  $node = isset($component['nid']) ? node_load($component['nid']) : NULL;

  // Add the main webform cloud upload js and css.
  $path = drupal_get_path('module', 'webform_cloud_upload');
  drupal_add_js($path . '/js/webform_cloud_upload.js');
  drupal_add_css($path . '/css/webform_cloud_upload.css');

  $element = array(
    '#type' => 'textarea',
    '#title' => $filter ? webform_filter_xss($component['name']) : $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#default_value' => $filter ? webform_replace_tokens($component['value'], $node) : $component['value'],
    '#required' => $component['required'],
    '#weight' => $component['weight'],
    '#field_prefix' => empty($component['extra']['field_prefix']) ? NULL : ($filter ? webform_filter_xss($component['extra']['field_prefix']) : $component['extra']['field_prefix']),
    '#field_suffix' => empty($component['extra']['field_suffix']) ? NULL : ($filter ? webform_filter_xss($component['extra']['field_suffix']) : $component['extra']['field_suffix']),
    '#theme_wrappers' => array('webform_element'),
    '#translatable' => array('title', 'description', 'field_prefix', 'field_suffix'),
  );

  if (isset($value[0])) {
    $element['#default_value'] = $value[0];
  }

  $js_settings['webformCloudSettings'] = array(
    'dropbox' => array(),
    'googleDrive' => array(),
  );

  $types = $component['extra']['filtering']['types'];
  $max_size = $component['extra']['filtering']['size'];
  $description = t('Files must be of type ' . $types . ' & no more than ' . $max_size);

  $info = array(
    'cardinality' => 1,
    'description' => $description,
    'multiselect' => '',
    // TODO make this more robust in case a user doesn't have a comma seperated list.
    'file-extentions' => $component['extra']['filtering']['types'],
    'max-filesize' => parse_size($max_size),
  );

  // Add the googledrive and dropbox settings to Drupal.behaviors.
  if (variable_get('webform_cloud_upload_dropbox', FALSE)) {
    drupal_add_js($path . '/js/dropbox.js');
    $js_settings['webformCloudSettings']['dropbox']['enabled'] = TRUE;
    $info['plugin'] = 'DropboxChooserAPI';
    $dropbox_app_key = variable_get('webform_cloud_upload_dropbox_app_key', '');

    // Add the required dropbox JS to the page.
    $html_element = array(
      '#type' => 'markup',
      '#markup' => '<script type="text/javascript" src="https://www.dropbox.com/static/api/2/dropins.js" id="dropboxjs" data-app-key="' . $dropbox_app_key . '"></script>' . "\r\n",
    );
    drupal_add_html_head($html_element, 'dropbox_dropin');

    // Build the link.
    $picker = theme('webform_cloud_upload_picker', array('info' => $info, 'label' => 'Dropbox',));
    $js_settings['webformCloudSettings']['dropbox']['picker'] = '<div class="dropbox-picker">' . $picker . '</div>';
  }

  // TODO everything for google drive.
  if (variable_get('webform_cloud_upload_google_drive', FALSE)) {
    $js_settings['webformCloudSettings']['googleDrive']['enabled'] = TRUE;
    $js_settings['webformCloudSettings']['googleDrive']['picker'] = '';
    // Add the google drive JS.
  }

  drupal_add_js($js_settings, 'setting');

  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_cloudupload($component, $value, $format = 'html') {
  $fids = isset($value[0]) ? unserialize($value[0]) : NULL;

  return array(
    '#title' => $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#weight' => $component['weight'],
    '#theme' => 'webform_cloud_upload_file',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#format' => $format,
    '#value' => $fids ? webform_cloud_upload_get_files($fids) : '',
    '#translatable' => array('title', 'field_prefix', 'field_suffix'),
  );
}

/**
 * Implements _webform_analysis_component().
 */
function _webform_analysis_cloudupload($component, $sids = array()) {
  $query = db_select('webform_submitted_data', 'wsd', array('fetch' => PDO::FETCH_ASSOC))
    ->fields('wsd', array('data'))
    ->condition('nid', $component['nid'])
    ->condition('cid', $component['cid']);

  if (count($sids)) {
    $query->condition('sid', $sids, 'IN');
  }

  $nonblanks = 0;
  $submissions = 0;
  $wordcount = 0;

  $result = $query->execute();
  foreach ($result as $data) {
    if (drupal_strlen(trim($data['data'])) > 0) {
      $nonblanks++;
      $wordcount += str_word_count(trim($data['data']));
    }
    $submissions++;
  }

  $rows[0] = array(t('Left Blank'), ($submissions - $nonblanks));
  $rows[1] = array(t('User entered value'), $nonblanks);

  $other[] = array(t('Average submission length in words (ex blanks)'), ($nonblanks != 0 ? number_format($wordcount/$nonblanks, 2) : '0'));

  return array(
    'table_rows' => $rows,
    'other_data' => $other,
  );
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_cloudupload($component, $value) {
  return check_plain(empty($value[0]) ? '' : $value[0]);
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_cloudupload($component, $export_options) {
  $header = array();
  $header[0] = '';
  $header[1] = '';
  $header[2] = $export_options['header_keys'] ? $component['form_key'] : $component['name'];
  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_cloudupload($component, $export_options, $value) {
  return !isset($value[0]) ? '' : $value[0];
}

/**
 * Implementation of _webform_submit_component().
 */
function _webform_submit_cloudupload($component, $value) {
  if (!empty($value)) {
    $file_urls = explode('|', $value);
    $validators = $component['extra']['filtering'];

    $element = array();
    $element['#validation']['file_validate_extensions'][] = $validators['types'];
    $element['#validation']['file_validate_size'][] = parse_size($validators['size']);
    $element['#schema'] = $component['extra']['scheme'];

    $fids = array();
    foreach ($file_urls as $key => $url) {
      $file = webform_cloud_upload_save_upload($element, $url);

      if ($file) {
        $fids[] = $file->fid;
      }
    }

    if (!empty($fids)) {
      return serialize($fids);
    }
  }
}

/**
 * Helper function to load a file from the database.
 */
function webform_cloud_upload_get_files($fids) {
  static $files;
  $allfiles = array();
  if (empty($fids)) return array();
  foreach ($fids as $fid) {
    if (!isset($files[$fid])) {
      if (empty($fid)) {
        $files[$fid] = FALSE;
      }
      else {
        $files[$fid] = file_load($fid);
      }
    }
    $allfiles[$fid] = $files[$fid];
  }

  return $allfiles;
}

/**
 * Save a linked file.
 *
 * @param array $element
 *   The upload element.
 *
 * @param string $url
 *   A linked cloud file URL.
 *
 * @return object $file
 *   The saved file object.
 *
 */
function webform_cloud_upload_save_upload($element, $url) {
  global $user;

  $clean_url = explode("::::", $url);
  $upload_location = $element['#schema'] . '://';

  // TODO specific directory?.
  if (!file_uri_scheme($upload_location)) {
    drupal_set_message('File location unavailable', 'error');
    return FALSE;
  }

  // Dropbox.
  if (strstr($url, 'DropboxChooserAPI')) {
    $downloaded_file_url = system_retrieve_file($clean_url[1], $upload_location);
  }

  // TODO Google drive.


  if ($downloaded_file_url) {
    $file_info = webform_cloud_upload_file_details(drupal_realpath($downloaded_file_url));

    // Build the file object.
    $file = new stdClass();
    $file->uid = $user->uid;
    $file->status = 1;
    $file->filename = trim(drupal_basename($file_info['filename']), '.');
    $file->uri = $downloaded_file_url;
    $file->filemime = file_get_mimetype($file->filename);
    $file->filesize = $file_info['filesize'];

    drupal_chmod($file->uri);
  }

  // Validate the file.
  $validation_errors = file_validate($file, $element['#validation']);

  if (!empty($validation_errors)) {
    foreach($validation_errors as $error) {
      drupal_set_message(t('The file could not be saved. ') . $error, 'error');
    }
  }

  else {
    // Save the file object to the database.
    $file = file_save($file);
    return $file;
  }

  // TODO security for .exe files

  return FALSE;
}

/**
 * Get the file information to prepare for upload.
 *
 * @param  string $downloaded_file_url
 *   The local URL of the retrieved file.
 *
 * @return  array $file_info
 *   File detail array.
 */
function webform_cloud_upload_file_details($downloaded_file_url) {
  $path = pathinfo($downloaded_file_url);

  $finfo = @finfo_open(FILEINFO_MIME_TYPE);
  $mimetype = @finfo_file($finfo, $path);
  $contents = file_get_contents($downloaded_file_url);

  $file_info = array(
    'filename'  => $path['basename'],
    'extension' => $path['extension'],
    'mimetype'  => $mimetype,
    'filesize'  => strlen($contents)
  );

  return $file_info;
}

/**
 * Format the output of data for this component.
 */
function theme_webform_cloud_upload_file($variables) {
  module_load_include('inc', 'webform', 'components/file');
  $element = $variables['element'];
  $output = '';

  if (!empty($element['#value'])) {
    $files = $element['#value'];

    foreach ($files as $file) {
      if (!empty($file)) {
        $url = webform_file_url($file->uri);
        $filename = urldecode($file->filename);

        if ($element['#format'] == 'html') {
          $output .= '<div class="multifile-file"> ';
          $output .= l($filename, $url);
          $output .= ' </div>';
        }
        else {
          $output .= $filename . ': ' . $url . "\n";
        }
      }
    }
  }

  return $output;
}

/**
 * Get a file extension from it's mimetype.
 *
 * @param string $mime
 *   A mime type
 *
 * @return string $extension
 *   A file extension
 */
// todo put this in its own inc.
// function webform_cloud_upload_get_file_extension($mime) {
//   $all_mimes = array(
//     'png':["image\/png","image\/x-png"],"bmp":["image\/bmp","image\/x-bmp","image\/x-bitmap","image\/x-xbitmap","image\/x-win-bitmap","image\/x-windows-bmp","image\/ms-bmp","image\/x-ms-bmp","application\/bmp","application\/x-bmp","application\/x-win-bitmap"],"gif":["image\/gif"],"jpeg":["image\/jpeg","image\/pjpeg"],"xspf":["application\/xspf+xml"],"vlc":["application\/videolan"],"wmv":["video\/x-ms-wmv","video\/x-ms-asf"],"au":["audio\/x-au"],"ac3":["audio\/ac3"],"flac":["audio\/x-flac"],"ogg":["audio\/ogg","video\/ogg","application\/ogg"],"kmz":["application\/vnd.google-earth.kmz"],"kml":["application\/vnd.google-earth.kml+xml"],"rtx":["text\/richtext"],"rtf":["text\/rtf"],"jar":["application\/java-archive","application\/x-java-application","application\/x-jar"],"zip":["application\/x-zip","application\/zip","application\/x-zip-compressed","application\/s-compressed","multipart\/x-zip"],"7zip":["application\/x-compressed"],"xml":["application\/xml","text\/xml"],"svg":["image\/svg+xml"],"3g2":["video\/3gpp2"],"3gp":["video\/3gp","video\/3gpp"],"mp4":["video\/mp4"],"m4a":["audio\/x-m4a"],"f4v":["video\/x-f4v"],"flv":["video\/x-flv"],"webm":["video\/webm"],"aac":["audio\/x-acc"],"m4u":["application\/vnd.mpegurl"],"pdf":["application\/pdf","application\/octet-stream"],"pptx":["application\/vnd.openxmlformats-officedocument.presentationml.presentation"],"ppt":["application\/powerpoint","application\/vnd.ms-powerpoint","application\/vnd.ms-office","application\/msword"],"docx":["application\/vnd.openxmlformats-officedocument.wordprocessingml.document"],"xlsx":["application\/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application\/vnd.ms-excel"],"xl":["application\/excel"],"xls":["application\/msexcel","application\/x-msexcel","application\/x-ms-excel","application\/x-excel","application\/x-dos_ms_excel","application\/xls","application\/x-xls"],"xsl":["text\/xsl"],"mpeg":["video\/mpeg"],"mov":["video\/quicktime"],"avi":["video\/x-msvideo","video\/msvideo","video\/avi","application\/x-troff-msvideo"],"movie":["video\/x-sgi-movie"],"log":["text\/x-log"],"txt":["text\/plain"],"css":["text\/css"],"html":["text\/html"],"wav":["audio\/x-wav","audio\/wave","audio\/wav"],"xhtml":["application\/xhtml+xml"],"tar":["application\/x-tar"],"tgz":["application\/x-gzip-compressed"],"psd":["application\/x-photoshop","image\/vnd.adobe.photoshop"],"exe":["application\/x-msdownload"],"js":["application\/x-javascript"],"mp3":["audio\/mpeg","audio\/mpg","audio\/mpeg3","audio\/mp3"],"rar":["application\/x-rar","application\/rar","application\/x-rar-compressed"],"gzip":["application\/x-gzip"],"hqx":["application\/mac-binhex40","application\/mac-binhex","application\/x-binhex40","application\/x-mac-binhex40"],"cpt":["application\/mac-compactpro"],"bin":["application\/macbinary","application\/mac-binary","application\/x-binary","application\/x-macbinary"],"oda":["application\/oda"],"ai":["application\/postscript"],"smil":["application\/smil"],"mif":["application\/vnd.mif"],"wbxml":["application\/wbxml"],"wmlc":["application\/wmlc"],"dcr":["application\/x-director"],"dvi":["application\/x-dvi"],"gtar":["application\/x-gtar"],"php":["application\/x-httpd-php","application\/php","application\/x-php","text\/php","text\/x-php","application\/x-httpd-php-source"],"swf":["application\/x-shockwave-flash"],"sit":["application\/x-stuffit"],"z":["application\/x-compress"],"mid":["audio\/midi"],"aif":["audio\/x-aiff","audio\/aiff"],"ram":["audio\/x-pn-realaudio"],"rpm":["audio\/x-pn-realaudio-plugin"],"ra":["audio\/x-realaudio"],"rv":["video\/vnd.rn-realvideo"],"jp2":["image\/jp2","video\/mj2","image\/jpx","image\/jpm"],"tiff":["image\/tiff"],"eml":["message\/rfc822"],"pem":["application\/x-x509-user-cert","application\/x-pem-file"],"p10":["application\/x-pkcs10","application\/pkcs10"],"p12":["application\/x-pkcs12"],"p7a":["application\/x-pkcs7-signature"],"p7c":["application\/pkcs7-mime","application\/x-pkcs7-mime"],"p7r":["application\/x-pkcs7-certreqresp"],"p7s":["application\/pkcs7-signature"],"crt":["application\/x-x509-ca-cert","application\/pkix-cert"],"crl":["application\/pkix-crl","application\/pkcs-crl"],"pgp":["application\/pgp"],"gpg":["application\/gpg-keys"],"rsa":["application\/x-pkcs7"],"ics":["text\/calendar"],"zsh":["text\/x-scriptzsh"],"cdr":["application\/cdr","application\/coreldraw","application\/x-cdr","application\/x-coreldraw","image\/cdr","image\/x-cdr","zz-application\/zz-winassoc-cdr"],"wma":["audio\/x-ms-wma"],"vcf":["text\/x-vcard"],"srt":["text\/srt"],"vtt":["text\/vtt"],"ico":["image\/x-icon","image\/x-ico","image\/vnd.microsoft.icon"],"csv":["text\/x-comma-separated-values","text\/comma-separated-values","application\/vnd.msexcel"],"json":["application\/json","text\/json"]}';

//   foreach ($all_mimes as $extension => $value) {
//     if (array_search($mime, $value) !== FALSE) {
//       return $extension;
//     }
//   }

//   return false;
// }
