/**
 * @file
 *
 * Dropbox upload function.
 */
(function ($) {
  'use strict';

  // Custom function to use the Dropbox picker with webforms.
  Drupal.behaviors.FileChooserFieldDropboxPickerWebformCustom = {
    attach: function (context, settings) {
      $('.webform-component-cloudupload .cloud-picker', context).unbind().click(function(e) {
        var $this = $(this);
        // Check if the existing file list has reached the upload limit.
        // if (fileLimitReached()) {
        //   displayFileUploadErrorMessage('limit');
        //   return false;
       // }

        var _plugin = $this.data('plugin'); // required
        var _max_filesize = $this.data('max-filesize');
        var _description = $this.data('description');
        var _extensions = $this.data('file-extentions');
        var _cardinality = $this.data('cardinality');
        var _multiselect = $this.data('multiselect');

        // Because there is no max files limit in Dropbox Chooser
        // we have to use this fake limitation.
        var _limit = (_cardinality > 0) ? _cardinality : 100;
        if (_limit > 1) {
          _multiselect = true;
        }

        // Dropbox plugin.
        Dropbox.choose({
          success: function(files) {
            var _links = [];
            var _count = 0;
            for (var i = 0; i < files.length; i++) {
              if (files[i].bytes > _max_filesize) {
                // File size is greater than the maimum allowed so strip the
                // file out.
                files.splice(i, 1);
                alert(_description);
              }
              else {
                if (_count < _limit) {
                  _links.push(_plugin + '::::' + files[i].link);
                  _count++;
                }
              }
            }

            var existingValue = $('.webform-component-cloudupload textarea').val();
            var countFiles = $('.webform-component-cloudupload .file-list').length;

            // // Get the max file upload limit.
            // if (Drupal.settings.webformUploadLimit) {
            //   var maxFiles = Drupal.settings.webformUploadLimit;
            // }

            // There are already some files in the list so merge them with the new files.
            if (existingValue.length) {
              var existingLinks = existingValue.split('|');
     //         _links = removeSurplus(_links, countFiles, maxFiles)

              _links = $.merge(existingLinks, _links);
            }

            // Set the file text area with the complete file list.
            $('.webform-component-cloudupload textarea').val(_links.join('|'));

            // Add the file to the file list.
            $.each(files, function(key, file) {
              // Need a check in here for max file.
              if (file.url) {
                file.link = file.url;
              }

              var cvRow = '<li><span class="uploaded-file"><a class="file-view-link" target="_blank" href="' + file.link + '">' + file.name + '</a></span>\n\
                <span class="upload-component"><a class="remove-link webform-file" href="javascript:void(0);" onclick="removeWebformFile(this)">' + Drupal.t('Remove') + '</a>\n\
                <a class="view-link" target="_blank" href="' + file.link + '">' + Drupal.t('View File') + '</a></span></li>';

              // Add this file to the list.
              $('.webform-component-cloudupload .file-list ul').append(cvRow);

              // Set the upload elements to ui-state-disabled if the limit has been reached.
              // if (fileLimitReached()) {
              //   $('.webform-component-filechooserui a, .webform-component-dragndrop .droppable').addClass('ui-state-disabled');
              // }
            });
          },

          extensions: _extensions.split(','),
          multiselect: _multiselect,
          linkType: 'direct'
        });
        e.preventDefault();
      });
    }
  };
})(jQuery);
