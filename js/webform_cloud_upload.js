(function ($) {
  'use strict';

  /**
   * Webform cloud upload.
   */
  Drupal.behaviors.webformCloudUpload = {
    attach: function (context, settings) {
      var config = Drupal.settings.webformCloudSettings;
      var $component = $('.webform-component-cloudupload');

      // Add an element to contain links to the file URLS.
      $('.webform-component-cloudupload').prepend('<div class="file-list"><ul></ul></div>')

      // Add the dropbox picker.
      if (config.dropbox.enabled) {
        var picker = config.dropbox.picker;
        // Build the settings into the picker.
        $component.prepend(picker);
      }

      if (config.googleDrive.enabled) {
        var chooser = '<div class="google-picker">drive</div>';
        // Build the settings into the chooser.
        $component.prepend(chooser);
      }
      // Add the googledrive picker.
      $component = $('.webform-component-cloudupload');
    }
  };
}(jQuery));
