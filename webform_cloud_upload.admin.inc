<?php
/**
 * @file
 * Administration pages webform cloud upload.
 */

/**
 * Configuration form for webform_cloud_upload.
 */
function webform_cloud_upload_admin_form($form_state) {
  $form = array();
   // Enable or disable the drag and drop functionality on specific forms.
  $form['dropbox'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dropbox configuration'),
  );

  $form['dropbox']['webform_cloud_upload_dropbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('Dropbox enabled'),
    '#default_value' => variable_get('webform_cloud_upload_dropbox', FALSE),
    '#description' => t('Enable dropbox as a file upload method.'),
  );

  $form['dropbox']['webform_cloud_upload_dropbox_app_key'] = array(
    '#title' => t('Dropbox App Key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('webform_cloud_upload_dropbox_app_key', ''),
    '#description' => t('Dropbox application key can be created/found <a href="https://www.dropbox.com/developers/apps">here</a>.')
  );

  $form['google_drive'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google drive configuration'),
  );

  $form['google_drive']['webform_cloud_upload_google_drive'] = array(
    '#type' => 'checkbox',
    '#title' => t('Google drive enabled'),
    '#default_value' => variable_get('webform_cloud_upload_google_drive', FALSE),
    '#description' => t('Enable google drive as a file upload method.'),
  );


  // $form['drag_and_drop']['dragndrop_cv_submit'] = array(
  //   '#type' => 'checkbox',
  //   '#title' => t('CV Submit form Drag and drop field'),
  //   '#default_value' => variable_get('dragndrop_cv_submit', FALSE),
  //   '#description' => t('Turn on the CV submit form Drag and drop field.'),
  // );

  // $form['drag_and_drop']['dragndrop_cv_manager'] = array(
  //   '#type' => 'checkbox',
  //   '#title' => t('CV manager Drag and drop field'),
  //   '#default_value' => variable_get('dragndrop_cv_manager', FALSE),
  //   '#description' => t('Turn on the CV manager Drag and drop field.'),
  // );

  // $form['drag_and_drop']['webform_drag_and_drop_upload_text'] = array(
  //   '#type' => 'textfield',
  //   '#title' => t('Webform drag and drop upload text'),
  //   '#default_value' => variable_get('webform_drag_and_drop_upload_text', t('or drag and drop a file here')),
  //   '#description' => t('The text to display in a webform drag and drop element'),
  // );

  return system_settings_form($form);
}
